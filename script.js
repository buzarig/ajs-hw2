"use strict";

let root = document.querySelector("#root");

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const validItems = ["author", "name", "price"];
const validBooks = [];

function validateBook() {
  books.forEach((book, bookIndex) => {
    try {
      for (let key of validItems) {
        if (!book[key]) {
          throw `missing ${key} in object with index:${bookIndex}`;
        }
      }
      validBooks.push(book);
    } catch (error) {
      console.error(error);
    }
  });
}
validateBook();

function createBooks() {
  validBooks.forEach((element) => {
    let list = document.createElement("ul");
    let item = document.createElement("li");
    for (let key of validItems) {
      item.innerText += ` ${element[key]} -`;
    }
    list.append(item);
    root.append(list);
  });
}
createBooks();
